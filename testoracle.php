<?php

$tns = "
(DESCRIPTION = 
    (ADDRESS_LIST =
        (ADDRESS = (PROTOCOL = TCP)(HOST = 10.1.106.252)(PORT = 1521))
    )
    (CONNECT_DATA =
        (SERVICE_NAME = uat)
    )
)
";
$dsn = "oci:dbname=$tns;charset=utf8";
$uid = "train_seal";
$pwd = "seal_pwd";
try {
    $pdo_object = new PDO($dsn, $uid, $pwd);
    $pdo_object->exec("SET CHARACTER SET utf8");
    $sql = "SELECT * FROM person";
    $result = $pdo_object->query($sql);
    
    while ($row=$result->fetch()) {
        echo $row['PRENAME'] ."&thinsp;". $row['FNAME'] ."&emsp;". $row['LNAME']."<br>";
    }
}
catch (PDOException $e) {
    echo 'Connection failed:' . $e->getMessage();
}

?>