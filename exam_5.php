<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Document</title>

<style>
   

.visible {
  visibility: visible;
  color: red;
}
.invisible {
  visibility: hidden;
}

   </style>


</head>
<body>

<form id="f_form" action="exam_json.php" method="get">
<!--<form id="f_form" action="" method="post">-->
<br/><br/> 



<div class="container">
<div class="form-group">

<div class="row">
  <div class="col-sm">ชื่อ*</div>
  <div class="col-sm">
  <input type="text" name="fname" class="form-control" id="fname" placeholder="กรุณาใส่ชื่อ" data-title="first name" maxlength=50>
  <span class="invisible">*ชื่อล่ะ</span>
  </div>
  <div class="col-sm">นามสกุล*</div>
  <div class="col-sm"><input type="text" name="lname" class="form-control" id="lname" placeholder="กรุณาใส่นามสกุล" data-title="last name" maxlength=50>
  <span class="invisible">*นามสกุลต้องมี</span>
  </div>
</div>
<div class="row">
  <div class="col-sm">ตำแหน่ง*</div>
  <div class="col-sm">
  <select name="position" id="position" class="form-control">
		<option value="" selected>กรุณาเลือก</option>
        <option value="1">Developer</option>
        <option value="2">SA</option>
        <option value="3">PM</option>

	</select>
    <span class="invisible">*ตำแหน่งต้องเลือกนะ</span>
  </div>
  <div class="col-sm"></div>
  <div class="col-sm"></div>
 
  
</div>



</form>
<div class="row">
    <div class="col-sm offset-sm-5">
    <button type="button" id="clear" class="btn btn-light">Clear</button>
    <button type="submit" id="save" class="btn btn-success">Save</button>
    </div>
</div>
</div>


<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

<script>
$(function () {
   // Name can't be blank
    $('#fname').on('input', function() {
	    var input=$(this);
	    var is_fname=input.val();

        
	    if(is_fname){
            input.removeClass("invalid").addClass("valid");
        }
	    else{
            input.removeClass("valid").addClass("invalid");
        }
    }); 

    // LastName can't be blank
    $('#lname').on('input', function() {
	    var input=$(this);
	    var is_lname=input.val();
	    if(is_lname){
            input.removeClass("invalid").addClass("valid");
        }
	    else{
            input.removeClass("valid").addClass("invalid");
        }
    }); 


    // Position can't be blank
    $('#position').on('input', function() {
	    var input=$(this);
	    var is_position=input.val();
	    if(is_position>="1"){
            input.removeClass("invalid").addClass("valid");
        }
	    else{
            input.removeClass("valid").addClass("invalid");
        }
    }); 

    //Clear All
    $("#clear").click(function (e) { 
            e.preventDefault();
            $('input').val("");
            $('select').val("");
          
            $('span').removeClass("visible").addClass("invisible");
            $('input').removeClass("valid").addClass("invalid");
            $('select').removeClass("valid").addClass("invalid");
            
            $('#fname').removeAttr('disabled');
            $('#lname').removeAttr('disabled');
            $('#position').removeAttr('disabled');
            $("#fname").focus();
            
            

    });

    $('form').submit(function(event){
        event.preventDefault(); 
	    var form_data=$("#f_form").serializeArray();        
	    var error_free=true;        

	    for (var input in form_data){
		    var element=$("#"+form_data[input]['name']);
		    var valid=element.hasClass("valid");
            var error_element=$("span", element.parent());
            error_element.removeClass("visible").addClass("invisible");
		    
		    if (!valid){
                error_element.removeClass("invisible").addClass("visible");
                error_free=false;
            }
            
		    else{
                error_element.removeClass("visible").addClass("invisible");
            }

	    }
        
	    if (!error_free){            
            event.preventDefault();
            return false;
	    }
	    
		    $.ajax({
                type: "GET",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "JSON",
                success: function (response) {
                   $('#fname').attr('disabled', true);
                   $('#lname').attr('disabled', true);
                   $('#position').attr('disabled', true);
                }
            });
      
	    
     
    });
 
});
</script>







<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
</body>
</html>